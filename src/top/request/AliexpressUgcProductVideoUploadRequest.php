<?php
namespace aliexpress\top\request;
use api\components\aliexpress\top\RequestCheckUtil;

/**
 * TOP API: aliexpress.ugc.product.video.upload request
 *
 * @author auto create
 * @since 1.0, 2020.08.11
 */
class AliexpressUgcProductVideoUploadRequest
{
	/**
	 * 表示更新主图视频
	 **/
	private $mediaType;

	/**
	 * 商品id
	 **/
	private $productId;

	/**
	 * 视频地址，后缀以视频格式结束，视频大小2G，视频格式wmv,avi,mpg,mpeg,3gp,mov,mp4,flv,f4v,m4v,m2t,mts,rmvb,vob,mkv
	 **/
	private $videoFileUrl;

	private $apiParas = array();

	public function setMediaType($mediaType)
	{
		$this->mediaType = $mediaType;
		$this->apiParas["media_type"] = $mediaType;
	}

	public function getMediaType()
	{
		return $this->mediaType;
	}

	public function setProductId($productId)
	{
		$this->productId = $productId;
		$this->apiParas["product_id"] = $productId;
	}

	public function getProductId()
	{
		return $this->productId;
	}

	public function setVideoFileUrl($videoFileUrl)
	{
		$this->videoFileUrl = $videoFileUrl;
		$this->apiParas["video_file_url"] = $videoFileUrl;
	}

	public function getVideoFileUrl()
	{
		return $this->videoFileUrl;
	}

	public function getApiMethodName()
	{
		return "aliexpress.ugc.product.video.upload";
	}

	public function getApiParas()
	{
		return $this->apiParas;
	}

	public function check()
	{

		RequestCheckUtil::checkNotNull($this->mediaType,"mediaType");
		RequestCheckUtil::checkNotNull($this->productId,"productId");
		RequestCheckUtil::checkNotNull($this->videoFileUrl,"videoFileUrl");
	}

	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}

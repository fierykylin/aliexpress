<?php
namespace aliexpress\top\domain;

/**
 * addresses
 * @author auto create
 */
class Addressdtos
{

	/**
	 * pickup address
	 **/
	public $pickup;

	/**
	 * receiver address
	 **/
	public $receiver;

	/**
	 * refund address
	 **/
	public $refund;

	/**
	 * sender address
	 **/
	public $sender;
}
?>

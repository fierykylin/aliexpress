<?php
namespace aliexpress\top\domain;

/**
 * 返回
 * @author auto create
 */
class PlainResult
{

	/**
	 * 数据
	 **/
	public $data;

	/**
	 * 消息
	 **/
	public $message;

	/**
	 * 是否成功
	 **/
	public $success;
}
?>
